"use strict";

// TODO Create object prototypes here
var musicAlbum = {
    title: "1989",
    artist: "Taylor Swift",
    year: 2014,
    tracks: [
        "Welcome to New York",
        "Blank Space",
        "Style",
        "Out of the Woods",
        "All you had to do was Stay",
        "Shake it Off",
        "I wish you Would",
        "Bad Blood",
        "Wildest Dreams",
        "How you get the Girl",
        "This Love",
        "I know Places",
        "Clean"
    ],
}


// TODO Create functions here
function MusicAlbum(title, artist, year) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.getAlbums = function () {
        return this.title + " released in " + this.year + " by " + this.artist;
    }
    this.printAlbums = function () {
        for (var i = 0; i < musicAlbum.tracks.length() - 1; i++) {
            console.log(musicAlbum.tracks[i]);
        }
    }
}

var newAlbum = new MusicAlbum ("1989", "Taylor Swift", 2014);
var newerAlbum = new MusicAlbum ("PTX Vol 1 - Classics", "Pentatonix", 2017);


// TODO Complete the program here
console.log(newAlbum.getAlbums());

console.log (newerAlbum.getAlbums());