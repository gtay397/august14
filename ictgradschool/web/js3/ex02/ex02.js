"use strict";

// TODO Car

var car = {
    year: 2007,
    make: "BMW",
    model: "323i",
    bodyType: "sedan",
    transmission: "triptronic",
    odometer: 68512,
    price: "16000",
}

// TODO Music

var musicAlbum = {
    title: "1989",
    artist: "Taylor Swift",
    year: 2014,
    genre: "pop",
    tracks: [
        "Welcome to New York",
        "Blank Space",
        "Style",
        "Out of the Woods",
        "All you had to do was Stay",
        "Shake it Off",
        "I wish you Would",
        "Bad Blood",
        "Wildest Dreams",
        "How you get the Girl",
        "This Love",
        "I know Places",
        "Clean"
    ],
    getAlbumDetails: function(){
        var albumDetails = this.title + ", released in " + this.year + " by " + this.artist;
        return albumDetails;
    }

}

var albumDetails = musicAlbum.getAlbumDetails()
console.log(albumDetails);
